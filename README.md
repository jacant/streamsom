# Self-Organizing Maps Streamlit Demo

## A SOM is a dimensionality reducer

## Steps in SOM optimization

1. Initialize values in of nodes in SOM grid to small values
2. Get input and calculate the distance between all nodes
and the input value.
3. Find location of node in the grid with the smallest distance.

4. To be continued..

``` python
import numpy as np

# setup random weight matrix
w = np.random.rand(50, 50, 3)
# an example random data point
x = np.random.rand(1, 3)

# how we vectorize extracting best node hit
best_node = np.unravel_index(
	np.square(w - x).sum(axis=2).argmin(), shape=(5,5)
)

```

[Link to SOM Tutorial](http://www.ai-junkie.com/ann/som/som1.html)
