#! /usr/bin/env python3

import numpy as np
import pickle

from bokeh.plotting import figure


class DataShapeError(Exception):
    pass


class NotInitializedError(Exception):
    pass


class SOM(object):
    def __init__(
        self, height=50, width=50, num_features=3,
        init_grid=True, initial_learning_rate=0.1, max_num_iterations=750
    ):
        self._h = height
        self._w = width
        self._n = num_features
        self.max_iters = max_num_iterations
        self.learning_rate = initial_learning_rate

        if init_grid:
            self.initialize_grid()
        else:
            self.grid = None
            self.initialized = False

    def _forward(self, data):
        self.get_bmu(data)

        # decay learning rate
        lr = self.learning_rate * np.exp(
            -self.niters/self.max_iters
        )
        # decay radius
        radius = self.initial_radius * np.exp(
            -0.5 * self.niters/self.time_const
        )
        self.update_grid(data, lr, radius)
        self.niters += 1

    def fit(self):
        self.initialize_grid()

    def get_bmu(self, data):
        if data.shape[-1] != self._n:
            raise DataShapeError(
                (
                    f"Shape of Input {data.shape[-1]} != "
                    f"Shape of Grid {self.grid.shape[-1]}"
                )
            )

        self.bmu_x, self.bmu_y = np.unravel_index(
            np.square(
                self.grid - data
            ).sum(axis=2).argmin(), shape=(self._h, self._w)
        )

    def get_dists_from_bmu(self):
        return (self._xs - self.bmu_x)**2 + (self._ys - self.bmu_y)**2

    def update_grid(self, data, learning_rate, radius):
        dist = self.get_dists_from_bmu()

        # get indices to update in grid
        update = dist < radius**2

        # move points in grid closer to data point in space
        theta = learning_rate * np.exp(
            -0.5 * dist[update] / (radius**2)
        ).reshape(-1, 1)
        self.grid[update] += theta * (data - self.grid[update])

    def initialize_grid(self):
        self.grid = np.random.rand(
            self._h, self._w, self._n
        )
        self._xs, self._ys = np.indices((self._h, self._w))
        self.niters = 0

        # setup initial radius and decay time constant
        self.initial_radius = max(self._h, self._w) / 2
        self.time_const = self.max_iters / np.log(self.initial_radius)

        self.initialized = True

    def __call__(self, data):
        if not(self.initialized):
            raise NotInitializedError(
                "Run initialize_grid method first!"
            )

        self._forward(data)

    def plot_grid(self, ht=750, wd=750):
        p = figure()
        p.plot_height = ht
        p.plot_width = wd

        p.grid.grid_line_color = None
        p.axis.axis_line_color = None
        p.axis.major_tick_line_color = None
        p.axis.minor_tick_line_color = None
        p.axis.major_label_text_font_size = '0pt'

        zipper = zip(self._xs.flatten(), self._ys.flatten())
        f = lambda x: '#%02x%02x%02x' % tuple(
            (self.grid[x]*255).round().astype(int)
        )
        colors = [f(z) for z in zipper]

        p.rect(
            self._xs.flatten(), self._ys.flatten(), 1, 1,
            color=colors, line_color='black', line_width=1
        )
        return p

    def save_model(self, output_name=None):
        if not(output_name):
            output_name = "./.default_som_model_state.pkl"
        outfile = open(output_name, "wb")
        pickle.dump(self.__dict__, outfile)
        outfile.close()

    @classmethod
    def load_model(cls, input_name=None):
        if not(input_name):
            input_name = "./.default_som_model_state.pkl"
        infile = open(input_name, "rb")
        new_inst = cls()
        new_inst.__dict__ = pickle.load(infile)
        infile.close()
        return new_inst
