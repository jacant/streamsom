#! /usr/bin/env python3

import streamlit as st
import numpy as np

from matplotlib import colors

from model.som import SOM

st.title("Self-Organizing Map Demo")

model_state_dict_path = "./streamlit_model_state.pkl"

model = SOM.load_model(model_state_dict_path)

st.bokeh_chart(model.plot_grid())

data = np.array([
    colors.BASE_COLORS[x] for x in colors.BASE_COLORS.keys()
])

step_size = 25
if st.button("Step", key="step_button"):
    for x in range(step_size):
        if (model.niters < model.max_iters):
            datum = data[np.random.choice(
                np.arange(data.shape[0])
            ), :]
            model(datum)
    model.save_model(output_name=model_state_dict_path)

h = st.slider(
    "Height of SOM Grid (number of rows)",
    min_value=20, max_value=100, value=64
)

w = st.slider(
    "Width of SOM Grid (number of columns)",
    min_value=20, max_value=100, value=64
)

n = st.slider(
    "Depth (data dimensionality) of SOM Grid",
    min_value=3, max_value=100, value=3
)

lr = st.slider(
    "The initial learning rate of the SOM",
    min_value=0.01, max_value=1.00, value=0.1
)

if st.button("Submit SOM", key="submit_som_button"):
    model = SOM(h, w, n, initial_learning_rate=lr)
    model.save_model(output_name=model_state_dict_path)


print(model.__dict__['niters'])

# st.bokeh_chart(model.plot_grid())
